FROM liliancal/ubuntu-php-apache
ADD src /var/www
EXPOSE 80
CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
